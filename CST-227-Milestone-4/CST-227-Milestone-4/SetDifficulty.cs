﻿//Difficulty popup window, William Thornton, CST-227, Milestone-5, Coded by William Thornton on 08/24/2019

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_227_Milestone_4
{
    public partial class SetDifficulty : Form
    {
        public int difficulty;
        public SetDifficulty()
        {
            InitializeComponent();
        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (easy.Checked)
            {
                difficulty = 10;
            }
            if (medium.Checked)
            {
                difficulty = 15;
            }
            if (hard.Checked)
            {
                difficulty = 20;
            }
            this.Close();
        }

        private void SetDifficulty_Load(object sender, EventArgs e)
        {

        }
    }
}
