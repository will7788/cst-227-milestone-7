﻿//Main minesweeper grid form, William Thornton, CST-227, Milestone-6, Coded by William Thornton on 08/31/2019
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_227_Milestone_4
{
    public partial class Minesweeper : Form
    {
        Random r = new Random();
        public int c = 0;
        public Cell[,] buttons;
        public int size;
        int time = 0;
        int counter = 0;
        
        public Minesweeper()
        {
            //create popup dialog before game starts to check difficulty
            SetDifficulty diff = new SetDifficulty();
            diff.ShowDialog();
            InitializeComponent();
            //get the public difficulty variable to set the grid size, later it will add more live tiles as a ratio
            buttons = new Cell[diff.difficulty, diff.difficulty];
            this.Size = new Size((buttons.GetUpperBound(0) + 1) * 42, (buttons.GetUpperBound(buttons.Rank - 1) + 1) * 49 + 2);
            size = diff.difficulty;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
            CreateButtons(buttons);
            ActivateBoard();
            FindNeighbors();
        }

        public void CreateButtons(Cell[,] buttons)
        {
            //assign rows and columns based on the bounds of the array we defined
            int rowNumber = buttons.GetUpperBound(0) + 1;
            int colNumber = buttons.GetUpperBound(buttons.Rank - 1) + 1;

            for (int i = 0; i < rowNumber; i++)
            {
                for (int j = 0; j < colNumber; j++)
                {
                    buttons[i, j] = new Cell(i, j);
                    buttons[i, j].Location = new Point(j * 40, 50 + (i * 40));
                    buttons[i, j].MouseDown += new MouseEventHandler(button_MouseClick);
                    Controls.Add(buttons[i, j]);
                }
            }
        }

        //sets the live tiles
        public void ActivateBoard()
        {
            Cell[] liveCells = new Cell[100];
            for (int i = 0; i < buttons.GetLength(0); i++)
            {
                for (int j = 0; j < buttons.GetLength(1); j++)
                {
                    if (r.Next(1, 101) < 15)
                    {
                        buttons[i, j].setLive(true);
                        buttons[i, j].setNeighbors(9);
                        liveCells[c] = buttons[i, j];
                        c += 1;
                    }
                }
            }
        }

        //get numbers for tiles that have live neighbors, by searching an 8x8 grid next to it
        public void FindNeighbors()
        {

            int rowLimit = buttons.GetLength(0);
            int columnLimit = buttons.GetLength(1);

            for (int i = 0; i < buttons.GetLength(0); i++)
            {
                for (int j = 0; j < buttons.GetLength(1); j++)
                {
                    if (buttons[i, j].getNeighbors() < 9)
                    {
                        int neighbors = 0;
                        for (int x = Math.Max(0, i - 1); x <= Math.Min(i + 1, rowLimit - 1); x++)
                        {
                            for (int y = Math.Max(0, j - 1); y <= Math.Min(j + 1, columnLimit - 1); y++)
                            {
                                if (x != i || y != j)
                                {
                                    bool test = buttons[x, y].getLive();
                                    if (test == true)
                                    {
                                        neighbors++;
                                    }
                                }
                            }
                        }
                        buttons[i, j].setNeighbors(neighbors);
                    }
                }
            }
        }

        //function runs anytime a button is clicked, checks neighbors to determine outcome
        private void revealNeighbors(int row, int column)
        {
            
            if (row < 0 || row >= size || column < 0 || column >= size)
            {
                return;
            }
            if (buttons[row, column].getNeighbors() < 9 && !buttons[row, column].getVisited())
            {
                if (buttons[row, column].getNeighbors() == 0)
                {
                    buttons[row, column].setVisited(true);
                    buttons[row, column].Text = buttons[row, column].getNeighbors().ToString();
                    counter++;
                    revealNeighbors(row + 1, column);
                    revealNeighbors(row - 1, column);
                    revealNeighbors(row, column - 1);
                    revealNeighbors(row, column + 1);
                    revealNeighbors(row + 1, column + 1);
                    revealNeighbors(row - 1, column - 1);
                    revealNeighbors(row + 1, column - 1);
                    revealNeighbors(row - 1, column + 1);
                }
                if (buttons[row, column].getNeighbors() > 0)
                {
                    buttons[row, column].Text = buttons[row, column].getNeighbors().ToString();
                    buttons[row, column].setVisited(true);
                    counter++;
                }

            }

            else if(buttons[row, column].getNeighbors() == 9)
            {
                for (int i = 0; i < buttons.GetLength(0); i++)
                {
                    for (int j = 0; j < buttons.GetLength(1); j++)
                    {
                        if (buttons[i, j].getLive())
                        {
                            buttons[i, j].Image = Image.FromFile("C:\\Users\\Will-Desktop\\source\\repos\\CST-227-Milestone-4\\CST-227-Milestone-4\\bomb1.png");
                        }
                    }
                }
                DialogResult dialog = MessageBox.Show("You hit a bomb, better luck next time!");
                if (dialog == DialogResult.OK)
                {
                    Application.Exit();
                }


            }

            if (counter + c == (size * size))

            {
                DialogResult dialog = MessageBox.Show("Winner winner chicken dinner! Your time in seconds: " + time);
                if (dialog == DialogResult.OK)
                {
                    WinCase();
                }
            }
        }

        //onclick function for buttons, checks left or right click
        private void button_MouseClick(object sender, MouseEventArgs e)
        {
            WinCase();
            Cell triggered = (Cell)sender;
            if (e.Button == MouseButtons.Right)
            {
                buttons[triggered.getRow(), triggered.getColumn()].Image = Image.FromFile("C:\\Users\\Will-Desktop\\source\\repos\\CST-227-Milestone-4\\CST-227-Milestone-4\\flag1.png");
            }
            else
            {
                revealNeighbors(triggered.getRow(), triggered.getColumn());
            }
        }

        //timer in app to display clock
        private void Timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = time.ToString();
            time++;
        }

        private void WinCase()
        {
            timer1.Stop();
            //creates the arrays necessary for storing the data from the file.
            string[] strarr = File.ReadAllLines("C:\\Users\\Will-Desktop\\Documents\\Visual Studio 2019\\highscores.txt");
            SortedList<int, string> scoreList = new SortedList<int, string>();
            //loop through the array pulled out of the file to separate numbers and names.
            foreach (var str in strarr)
            {
                string tempname = Regex.Replace(str, @"[\d-]", string.Empty);
                int tempnum = Int32.Parse(Regex.Replace(str, "[^0-9.]", ""));
                scoreList.Add(tempnum, tempname);
            }
            

            UserInfo user = new UserInfo();
            user.ShowDialog();
            string name = user.name;
            HighScores hs = new HighScores();
            scoreList.Add(time, name);
            Label[] labels = new Label[10] { hs.label1, hs.label2, hs.label3, hs.label4, hs.label5, hs.label6, hs.label7, hs.label8, hs.label9, hs.label10 };
            for (int i = 0; i < 5; i++)
            {
                KeyValuePair<int, string> kvp = scoreList.ElementAt(i);
                labels[i].Text = kvp.Value;
                labels[i + 5].Text = kvp.Key + "";
            }

            hs.ShowDialog();
            StreamWriter outFile = new StreamWriter("C:\\Users\\Will-Desktop\\Documents\\Visual Studio 2019\\highscores.txt");
            for (int i = 0; i<strarr.Length;i++)
            {
                string fileInput = scoreList.ElementAt(i).Value + scoreList.ElementAt(i).Key;
                strarr[i] = fileInput;
                outFile.WriteLine(fileInput);
            }
            outFile.Close();
            Application.Exit();


        }


    }
}
